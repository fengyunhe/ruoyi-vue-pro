CREATE TABLE IF NOT EXISTS bpm_user_group
(
    id bigint NOT NULL auto_increment,
    name        varchar(63)  NOT NULL,
    description varchar(255) NOT NULL,
    status      tinyint      NOT NULL,
    user_ids    varchar(255) NOT NULL,
    creator     varchar(64)           DEFAULT '',
    create_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater     varchar(64)           DEFAULT '',
    update_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted     bit          NOT NULL DEFAULT FALSE,
    `tenant_id` bigint NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '用户组';

CREATE TABLE IF NOT EXISTS bpm_category
(
    id          bigint       NOT NULL auto_increment,
    name        varchar(63)  NOT NULL,
    code        varchar(63)  NOT NULL,
    description varchar(255) NULL,
    status      tinyint      NOT NULL,
    sort        int          NOT NULL,
    creator     varchar(64)           DEFAULT '',
    create_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater     varchar(64)           DEFAULT '',
    update_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted     bit          NOT NULL DEFAULT FALSE,
    `tenant_id` bigint NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '分类';

CREATE TABLE IF NOT EXISTS bpm_form
(
    id     bigint   NOT NULL auto_increment,
    name        varchar(63)  NOT NULL,
    status      tinyint      NOT NULL,
    fields longtext NOT NULL,
    conf        varchar(255) NOT NULL,
    remark      varchar(255),
    creator     varchar(64)           DEFAULT '',
    create_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater     varchar(64)           DEFAULT '',
    update_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted     bit          NOT NULL DEFAULT FALSE,
    `tenant_id` bigint NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '动态表单';

CREATE TABLE IF NOT EXISTS bpm_process_listener
(
    id bigint NOT NULL auto_increment,
    name        varchar(63)  NOT NULL,
    status      tinyint      NOT NULL,
    type        varchar(255) NOT NULL,
    event       varchar(255) NOT NULL,
    value_type  varchar(255) NOT NULL,
    value       varchar(255),
    creator     varchar(64)           DEFAULT '',
    create_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater     varchar(64)           DEFAULT '',
    update_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted     bit          NOT NULL DEFAULT FALSE,
    `tenant_id` bigint       NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '流程监听器';

CREATE TABLE IF NOT EXISTS bpm_process_expression
(
    id bigint NOT NULL auto_increment,
    name        varchar(63)  NOT NULL,
    status      tinyint      NOT NULL,
    expression  varchar(255) NOT NULL,
    creator     varchar(64)           DEFAULT '',
    create_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater     varchar(64)           DEFAULT '',
    update_time timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted     bit          NOT NULL DEFAULT FALSE,
    `tenant_id` bigint       NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '流程表达式';

CREATE TABLE IF NOT EXISTS bpm_process_definition_info
(
    id                      bigint        NOT NULL auto_increment,
    process_definition_id   varchar(64)   NOT NULL,
    model_id                varchar(255)  NOT NULL,
    icon                    varchar(255)  NOT NULL,
    description             varchar(255)  NULL,
    form_type               int           NULL,
    form_id                 int           NULL,
    form_conf               longtext,
    form_fields             longtext,
    form_custom_create_path varchar(1024) null,
    form_custom_view_path   varchar(1024) null,
    creator                 varchar(64)            DEFAULT '',
    create_time             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater                 varchar(64)            DEFAULT '',
    update_time             timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted                 bit           NOT NULL DEFAULT FALSE,
    `tenant_id`             bigint        NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '流程定义的拓信息';

CREATE TABLE IF NOT EXISTS bpm_process_instance_copy
(
    id                    bigint        NOT NULL auto_increment,
    start_user_id         int           not NULL,
    process_instance_name varchar(255) not null,
    process_instance_id   varchar(64) not null,
    category              varchar(64) not null,
    task_id               varchar(1024) not null,
    task_name             varchar(1024) not null,
    user_id               int           not null,
    creator               varchar(64)            DEFAULT '',
    create_time           timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater               varchar(64)            DEFAULT '',
    update_time           timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted               bit           NOT NULL DEFAULT FALSE,
    `tenant_id`           bigint        NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT 'BPM 流程抄送';

CREATE TABLE IF NOT EXISTS bpm_user_group
(
    id          bigint        NOT NULL auto_increment,
    name        varchar(64) not null,
    description varchar(1024) not null,
    status      tinyint       NOT NULL,
    user_ids    longtext      not null,
    creator     varchar(64)            DEFAULT '',
    create_time timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater     varchar(64)            DEFAULT '',
    update_time timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted     bit           NOT NULL DEFAULT FALSE,
    `tenant_id` bigint        NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '用户组';

CREATE TABLE IF NOT EXISTS bpm_oa_leave
(
    id                  bigint        NOT NULL auto_increment,
    user_id             int           not null,
    type                varchar(255)  NOT NULL,
    reason              varchar(1024) not null,
    start_time          datetime      not null,
    end_time            datetime      not null,
    status              tinyint       NOT NULL,
    day                 int           not null,
    process_instance_id varchar(64)   null,
    creator             varchar(64)            DEFAULT '',
    create_time         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updater             varchar(64)            DEFAULT '',
    update_time         timestamp     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted             bit           NOT NULL DEFAULT FALSE,
    `tenant_id`         bigint        NOT NULL DEFAULT 0 COMMENT '租户编号',
    PRIMARY KEY (id)
) COMMENT '请假申请';
